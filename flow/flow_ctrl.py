"""
Flow reading script.
In fact, it counts how many times the sensor detects flow in a sec.
"""
import os
import threading
import time


try:
    is_rpi = True if os.uname()[4][:3] == 'arm' else False
except AttributeError:
    is_rpi = False

if is_rpi:
    import RPi.GPIO as GPIO


class FlowCtrl:

    flow_pin = 22
    count = 0
    lock = threading.RLock()

    def __init__(self, flow_pin=22):
        self.flow_pin = flow_pin
        self.count = 0
        self.initialize_gpio()

    def initialize_gpio(self):
        """
        Initialize GPIO settings
        :return:
        """
        if is_rpi:
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self.flow_pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    def count_pulse(self, *args):
        with self.lock:
            self.count += 1

    def start_sensoring(self):
        if is_rpi:
            GPIO.add_event_detect(self.flow_pin, GPIO.FALLING, callback=self.count_pulse)

        threading.Thread(target=self.start_count).start()

    def start_count(self):
        while True:
            with self.lock:
                self.count = 0       # Initialize `count` every second
            time.sleep(1)

    def get_flow(self):
        with self.lock:
            return self.count


if __name__ == '__main__':

    a = FlowCtrl()

    a.start_sensoring()

    while True:

        time.sleep(.5)
        print 'Flow: ', a.get_flow()
