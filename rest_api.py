import requests


if __name__ == '__main__':

    url = 'http://handcert.herokuapp.com/api/v1/rpi_events'
    post_data = {"uuid": "Test UUID", "minor": 3, "macid": "aa:bb:cc:dd:ee:ff", "zone": "OUTER", "zone_action": "AUTH",
                 "scanner_name": "Test Scanner", "distance": 10, "major": 543,
                 "sent_time": "2017-02-12T12:07:29.445-06:00", "tx_power": -111, "rssi": -60,
                 "scanner_mac": "11:22:33:44:55:66"}
    headers = {'content-type': 'application/json'}

    r = requests.post(url, json=post_data)
    print r.content
