import logging
import os
import socket
import time
import boto
import datetime
from boto.s3.key import Key


try:
    is_rpi = True if os.uname()[4][:3] == 'arm' else False
except AttributeError:
    is_rpi = False


UPLOAD_INTERVAL = 20

aws_bucket = 'psychtrac-attendance'
aws_key = 'AKIAILTNHX7RRALXPD3A'
aws_security = 'qj564q3tWCpaVrMDD/YY/oE2uR/D8vPRMm9JW4o6'

log_file_name = 'log.txt'
logging.basicConfig(level=10, filename=log_file_name, format='%(asctime)s: %(message)s',
                    datefmt='%m/%d/%Y %H:%M:%S')
logging.getLogger('boto').setLevel(logging.ERROR)


def push_picture_to_s3(file_key, file_path):
    """
    Upload file to AWS S3 bucket
    :param file_key:
    :param file_path:
    :return:
    """
    try:
        # connect to the bucket
        conn = boto.connect_s3(aws_key, aws_security)
        bucket = conn.get_bucket(aws_bucket)

        msg = "Uploading to AWS.. Key: " + file_key + "  Path: " + file_path
        print msg
        logging.info(msg)

        k = Key(bucket)
        k.key = file_key
        k.set_contents_from_filename(file_path)
        # we need to make it public so it can be accessed publicly
        # using a URL like http://s3.amazonaws.com/bucket_name/key
        k.make_public()
        print "Succeeded to upload to AWS S3..."
        logging.info("Succeeded to upload to AWS S3...")
        # for key in bucket.list():
        #     print key.name
        return True
    except ValueError as er:
        print er
        logging.error(er)
        return False
    except socket.gaierror as er:
        print er
        logging.error(er)
        return False
    except boto.exception.S3ResponseError as er:
        print er
        logging.error(er)
        return False


def download_all():
    conn = boto.connect_s3(aws_key, aws_security)
    bucket = conn.get_bucket(aws_bucket)
    for key in bucket.list():
        f_name = str(key.key)
        print 'Downloading {}'.format(key.name)
        key.get_contents_to_filename(f_name)


def delete(file_key):
    conn = boto.connect_s3(aws_key, aws_security)
    bucket = conn.get_bucket(aws_bucket)
    bucket.delete_key(file_key)


def get_mac(interface):
    # Return the MAC address of interface
    try:
        _str = open('/sys/class/net/' + interface + '/address').read()
    except:
        _str = "00:00:00:00:00:00"
    return _str[0:17]


if __name__ == '__main__':

    while True:
        if is_rpi:
            file_name = datetime.datetime.now().strftime("webcam_%m_%d_%Y__%H_%M_%S") + '.jpg'
            os.system('fswebcam -r 1280x720 {}'.format(file_name))
            if push_picture_to_s3(file_key=get_mac('wlan0').replace(':', '') + '/' + file_name, file_path=file_name):
                os.system('rm {}'.format(file_name))
            file_name = datetime.datetime.now().strftime("picam_%m_%d_%Y__%H_%M_%S") + '.jpg'
            os.system('raspistill -o {}'.format(file_name))
            if push_picture_to_s3(file_key=get_mac('wlan0').replace(':', '') + '/' + file_name, file_path=file_name):
                os.system('rm {}'.format(file_name))
            time.sleep(UPLOAD_INTERVAL)
        else:       # For test
            # push_picture_to_s3(file_key='test_MAC/' + file_name, file_path='sample.png')
            download_all()
            break
